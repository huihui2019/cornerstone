package com.cs.base.quartz.core;

import com.cs.base.common.SpringBootContext;
import lombok.Data;
import lombok.extern.slf4j.Slf4j;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.springframework.context.ApplicationContext;
import org.springframework.scheduling.quartz.QuartzJobBean;

import java.lang.reflect.Method;

/**
 * @author wangjiahao
 * @version 1.0
 * @className DetailJobBean
 * @since 2019-03-17 16:36
 */
@Slf4j
@Data
public class DetailJobBean extends QuartzJobBean {

    private String targetObject;
    private String targetMethod;
    private ApplicationContext ctx = SpringBootContext.getApplicationContext();

    @Override
    protected void executeInternal(JobExecutionContext context) throws JobExecutionException {
        try {
            Object otargetObject = ctx.getBean(targetObject);
            Method m = null;
            log.debug("任务开始执行");
            try {
                m = otargetObject.getClass().getMethod(targetMethod, new Class[] { JobExecutionContext.class });
                m.invoke(otargetObject, new Object[] { context });
            } catch (SecurityException e) {
                log.error("",e);
            } catch (NoSuchMethodException e) {
                log.error("",e);
            }
        } catch (Exception e) {
            throw new JobExecutionException(e);
        }
    }
}
