/**
 * Copyright (C), 2015-2019, XXX有限公司
 * FileName: DemoElasticSearchApplication
 * Author:   liyuan
 * Date:     2019-03-18 17:57
 * Description:
 * History:
 * <author>          <time>          <version>          <desc>
 * 作者姓名           修改时间           版本号              描述
 */
package com.cs.micro.demo.elasticsearch;

import com.codingapi.txlcn.tc.config.EnableDistributedTransaction;
import org.springframework.cloud.client.SpringCloudApplication;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.cloud.openfeign.EnableFeignClients;

/**
 * 〈一句话功能简述〉<br> 
 * 〈〉
 *
 * @author liyuan
 * @create 2019-03-18
 * @since 1.0.0
 */
@RefreshScope
@SpringCloudApplication
@EnableDistributedTransaction
@EnableFeignClients(basePackages = "com.cs.micro.demo.elasticsearch.api")
public class DemoElasticSearchApplication {

}